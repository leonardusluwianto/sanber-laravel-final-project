<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Blog Post - Start Bootstrap Template</title>

  <!-- Bootstrap core CSS -->
  <link href="{{ asset('/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="{{ asset('/css/blog-post.css') }}" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="#">SanberCode Twelve Social Media</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Services</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Contact</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Content -->
  <div class="container">

    <div class="row">

      <!-- Post Content Column -->
      <div class="col-lg-8">

        <!-- Title -->
        <h1 class="mt-4">Menulis Program Berorientasi Objek</h1>

        <!-- Author -->
        <p class="lead">
          by
          <a href="#">Leonardus Brandon </a>
        </p>

        <hr>

        <!-- Date/Time -->
        <p>Posted on February 07, 2021 at 12:00 PM</p>

        <hr>

        <!-- Preview Image -->
        <img class="img-fluid rounded" src="{{ asset('/article_image.jpeg') }}" alt="">

        <hr>

        <!-- Post Content -->
        <small>Sumber gambar: https://miro.medium.com/max/5000/1*TpcMd-ZIi6Iea6_vYxYtbQ.jpeg</small>
        <br><br>
        <h5>Bahasa Pemrograman Berorientasi Objek</h5>
        <p>Bahasa pemrograman berorientasi objek dapat dibagi menjadi tiga kategori, yaitu bahasa yang sejak awal dirancang sebagai bahasa berorientasi objek (e.g. Python, Ruby, Scala, Eiffel, Self, Swift), bahasa yang merupakan tambahan lapisan berorientasi objek di atas keluarga bahasa pemrograman yang sudah ada (e.g. Java, C++, C#, Objective-C, Delphi/Object Pascal, Visual Basic), dan ada beberapa bahasa (e.g. Groovy dan Kotlin) yang dibuat untuk menyederhanakan sintaks dan keterbatasan bahasa Java yang dianggap merepotkan.</p>
        
        <p>Dalam bahasa pemrograman berorientasi objek, terdapat sekumpulan objek dan kelas yang telah terdefinisi dan secara umum disebut sebagai standard library atau core library (pustaka inti) yang merupakan perwujudan konsep objek yang dapat digunakan kembali (reusable) sehingga dalam paradigma objek menulis program dapat dilakukan hanya dengan merangkai objek-objek yang sudah ada. Contoh kumpulan kelas itu adalah String, Array, File, Date, Time, Socket, dll.</p>
        
        <h5>Menulis Program dalam Bahasa Berorientasi Objek vs. Menulis Program Berparadigma Objek</h5>

        <p>Menulis program dalam bahasa berorientasi objek tidak secara otomatis membuat program tersebut memenuhi paradigma objek, bisa saja bahasa berorientasi objek yang ditulis tidak menggunakan paradigma objek, tetapi masih berupa urutan instruksi (paradigma prosedural). Dalam paradigma prosedural, alur kendali (control flow) seperti if-then, if-then-else, while-do, dan traversal for-i-from-x-to-y umum digunakan, sedangkan dalam paradigma objek, alur kendali untuk perulangan tidak dibutuhkan.</p>

        <h5>Menulis Program dengan Paradigma Objek</h5>

        <p>Dalam menulis program dengan sistem berbasis kelas, pemrogram memiliki dua peran yaitu architect (arsitek) yang membuat cetak biru dan builder (kontraktor) yang menciptakan objek-objek sesuai cetak biru, merangkai, dan memastikan objek satu dengan lainnya direkatkan dengan benar. Pemrogram perlu berpikir sesuai peranan yang diambil, baik architect maupun builder, jangan sampai mencampurkan kedua peran tersebut agar tidak mengacaukan rancangan perangkat lunak.</p>

        <p>Seorang architect bertugas 1. mengidentifikasi objek-objek yang ada pada ruang persoalan yang ingin diselesaikan, 2. menentukan tanggung jawab (metode) yang dimiliki setiap objek, dan 3. mengolaborasikan objek-objek terkait (atribut & variabel/parameter) untuk memenuhi tanggung jawab tersebut. Pola pikir yang harus dimiliki dalam proses pengembangan perangkat lunak menggunakan paradigma objek adalah maintainability, yaitu agar program lebih bersih dan mudah di-maintain kendati harus menulis ulang kelas baru atau membuang beberapa kelas.</p>

        <p>Seorang builder bertugas 1. menciptakan objek-objek, 2. mengoordinasikan melalui pengiriman pesan, hingga membentuk suatu program lengkap yang menyelesaikan sebuah masalah, dua proses tersebut disebut dengan scripting atau menulis program utama. Script dalam paradigma objek biasanya sangat singkat, umumnya hanya menciptakan sedikit objek dan mengirimi pesan kepada salah satunya sebagai inisiator yang kemudian merespons pesan tersebut dengan menciptakan objek-objek lainnya, berbeda dengan paradigma prosedural yang melibatkan alur program pada tingkat abstraksi tertinggi.</p>

        <hr>

        <!-- Comments Form -->
        <div class="card my-4">
          <h5 class="card-header">Leave a Comment:</h5>
          <div class="card-body">
            <form>
              <div class="form-group">
                <textarea class="form-control" rows="3"></textarea>
              </div>
              <button type="submit" class="btn btn-primary">Submit</button>
            </form>
          </div>
        </div>

        <!-- Single Comment -->
        <div class="media mb-4">
          <img class="d-flex mr-3 rounded-circle" src="http://placehold.it/50x50" alt="">
          <div class="media-body">
            <h5 class="mt-0">Unknown</h5>
            Artikel yang menarik!<br>
            Likes: 0        Comments: 0
          </div>
        </div>

        <!-- Comment with nested comments -->
        

      </div>

      <!-- Sidebar Widgets Column -->
      <div class="col-md-4">

        <!-- Search Widget -->
        <div class="card my-4">
          <h5 class="card-header">Search</h5>
          <div class="card-body">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Search for...">
              <span class="input-group-append">
                <button class="btn btn-secondary" type="button">Go!</button>
              </span>
            </div>
          </div>
        </div>

        <!-- Categories Widget -->
        <div class="card my-4">
          <h5 class="card-header">Categories</h5>
          <div class="card-body">
            <div class="row">
              <div class="col-lg-6">
                <ul class="list-unstyled mb-0">
                  <li>
                    <a href="#">Web Design</a>
                  </li>
                  <li>
                    <a href="#">HTML</a>
                  </li>
                  <li>
                    <a href="#">Freebies</a>
                  </li>
                </ul>
              </div>
              <div class="col-lg-6">
                <ul class="list-unstyled mb-0">
                  <li>
                    <a href="#">JavaScript</a>
                  </li>
                  <li>
                    <a href="#">CSS</a>
                  </li>
                  <li>
                    <a href="#">Tutorials</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>

        <!-- Side Widget -->
        

      </div>

    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->

  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Your Website 2020</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="{{ asset('/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

</body>

</html>
